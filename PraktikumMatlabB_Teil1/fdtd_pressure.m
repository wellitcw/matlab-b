function p_out = fdtd_pressure(p_init, c, delta_z, T, N_z, N_t, edge_condition)
% edge_condition assignment: 
% 1 - schallweich (default)
% 2 - schallhart
% 3 - absorbierend
% other - error

%% control all the parameters and throw an errer or adjust them if necessary

% edge_condition
if ~exist('edge_condition', 'var')
    edge_condition = 1;
end
if numel(edge_condition) ~= 1
    error('edge_condition must be scalar. Use ONE of these settings (or leave blank): \n1 - schallweich (default) \n2 - schallhart \n3 - absorbierend%s', '');
elseif ~isnumeric(edge_condition)
    error('edge_condition must be a number. Use ONE of these settings (or leave blank): \n1 - schallweich (default) \n2 - schallhart \n3 - absorbierend%s', '');
elseif ~ismember(edge_condition, [1 2 3])
    error('using not supported edge condition (%s). Try one of these settings (or leave blank):\n1 - schallweich (default) \n2 - schallhart \n3 - absorbierend',num2str(edge_condition));
end

% N_z
if numel(N_z) ~= 1
    error('N_z must be scalar');
end
N_z = round(N_z); % tackle deviations caused by numeric issues

% N_t
if numel(N_t) ~= 1
    error('N_t must be scalar');
end
N_t = round(N_t); % tackle deviations caused by numeric issues

% c
if numel(c) ~=1 && ~(isvector(c) && numel(c) == N_z)
    error('c must be scalar (homogen) or have N_z elements');
end

% delta_z
if numel(delta_z) ~= 1
    error('delta_z must be scalar');
end

% T
if numel(T) ~= 1
    error('T must be scalar');
end

% p_init
if ~isvector(p_init) % padd p_init if necessary  
    error('p_init must be vector');
end
if size(p_init, 1)~=1 % turn p_init in row vector
    p_init = p_init.';
end
if length(p_init) > N_t %truncate p_init to fit N_t if necessary
    p_init = p_init(1:N_t); 
end
p_init = [p_init, zeros(1, N_t-length(p_init))]; % pad with zeros

%% prepare for iterative computation
p_out=zeros(N_t, N_z, 'double'); %initialize
p_out(:, 1)=p_init.'; % insert source
s=c*T/delta_z; %courant number

if numel(s) == 1
    s=s*ones(1, N_z);
end

%% compute the precious pressures

% compute for p(j=1) separatley, because the ordinary equation would try to
% use p(j=-1) and therefore try to access p_out with an index 0
% on top of which for j=1, only p(n=1, j=1) needs to be computed as the
% sound cannot have travveld any further. So the 0s in p_out can be left
% untoucht. This is used in the inner loop further down as well.
p_out(2,2) = s(2)^2*p_out(1, 1);

for n=3:N_t
    for j=2:min(N_z, n) % sound cannot have travelled further
        if j< N_z
            p_out(n, j) = s(j)^2*p_out(n-1, j-1) + 2*(1-s(j)^2)*p_out(n-1, j) + s(j)^2*p_out(n-1, j+1) - p_out(n-2, j);
        else
            switch edge_condition
                case 1
                    % nothing to be done (leave p(n, J-1)=0)
                case 2
                    p_out(n, j) = 2*s(j)^2*p_out(n-1, j-1) + 2*(1-s(j)^2)*p_out(n-1, j) - p_out(n-2, j);
                case 3
                    p_out(n, j) = p_out(n-1, j-1) +(2-s(j)-s(j-1))/(2+s(j)+s(j-1))*(p_out(n-1, j) - p_out(n, j-1));
            end
        end
        
    end %j loop
end %n looop

end

