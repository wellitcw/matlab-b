clear all
[pic_file_name, pic_file_path] = uigetfile ({ '*.jpg;*.jpeg;*.png','JPEG or PNG';'*.jpg;*.jpeg','JPEG';'*.png','PNG' }, 'Pic an image');
if ~ischar(pic_file_name) || ~ischar(pic_file_path)
    return
end

image = imread([pic_file_path, pic_file_name]);

tic;
extracted = extractM(image);
timeM = toc;

tic;
extracted = extractC(image);
timeC = toc;

file = fopen('extracted.txt', 'w');
fwrite(file, char(extracted));
fclose(file);

extractedimage = base64decode(extracted);

file = fopen('extracted.jpg','w');
fwrite(file,extractedimage);
fclose(file);