function [outPic] = thresholdFilter(green_pic, background, threshold)

outPic = green_pic;

for index1 = 1:size(green_pic, 1)
    for index2 = 1:size(green_pic, 2)
        if green_pic(index1, index2, 2) > threshold
            outPic(index1, index2, :) = background(index1, index2, :);
        end
    end 
end
end

