function varargout = Kantendetektion(varargin)
% KANTENDETEKTION MATLAB code for Kantendetektion.fig
%      KANTENDETEKTION, by itself, creates a new KANTENDETEKTION or raises the existing
%      singleton*.
%
%      H = KANTENDETEKTION returns the handle to a new KANTENDETEKTION or the handle to
%      the existing singleton*.
%
%      KANTENDETEKTION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in KANTENDETEKTION.M with the given input arguments.
%
%      KANTENDETEKTION('Property','Value',...) creates a new KANTENDETEKTION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Kantendetektion_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Kantendetektion_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Kantendetektion

% Last Modified by GUIDE v2.5 04-May-2019 19:21:14

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Kantendetektion_OpeningFcn, ...
                   'gui_OutputFcn',  @Kantendetektion_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Kantendetektion is made visible.
function Kantendetektion_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Kantendetektion (see VARARGIN)

% Choose default command line output for Kantendetektion
handles.output = hObject;

global lambda;
global Graybild;
Graybild = NaN;
lambda = 0.2; %defaultwert
set(handles.sl_lambda, 'Value', lambda);
set(handles.lb_lambda, 'String', num2str(lambda,2));

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Kantendetektion wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Kantendetektion_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in btn_load.
function btn_load_Callback(hObject, eventdata, handles)
% hObject    handle to btn_load (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Graybild;

[pic_file_name, pic_file_path] = uigetfile ({ '*.jpg;*.jpeg','JPEG';'*.png','PNG' }, 'Pic an image');
if ~ischar(pic_file_name) || ~ischar(pic_file_path)
    return
end

Graybild = imread([pic_file_path, pic_file_name]);
if ndims(Graybild)==3
    Graybild = rgb2gray(Graybild);
end
imshow(Graybild, 'Parent', handles.axes_gray);


% --- Executes on slider movement.
function sl_lambda_Callback(hObject, eventdata, handles)
% hObject    handle to sl_lambda (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global lambda;
lambda = get(hObject,'Value');
set(handles.lb_lambda, 'String', num2str(lambda,2));


% --- Executes during object creation, after setting all properties.
function sl_lambda_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sl_lambda (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in btn_ortsraumIgnore.
function btn_ortsraumIgnore_Callback(hObject, eventdata, handles)
% hObject    handle to btn_ortsraumIgnore (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Graybild;
global Laplacebild;
global Diffbild;
global lambda;

if (any(isnan(Graybild)))
    msgbox("Graubild nicht geladen");
    return
end

tic;
Laplacebild = edge_ignore(Graybild, [0 1 0;1 -4 1; 0 1 0]);
time=toc;
imshow(Laplacebild, 'Parent', handles.axes_laplace);
set(handles.lb_timeOrtsraumIgnore, 'String', join(["Zeit im Ortsraum (Ignore): ", num2str(time*1e3,5), "ms"], ""));

Diffbild = Graybild(2:end-1, 2:end-1) - uint8(lambda * Laplacebild);
imshow(Diffbild, 'Parent', handles.axes_diff);


% --- Executes on button press in btn_ortsraumZero.
function btn_ortsraumZero_Callback(hObject, eventdata, handles)
% hObject    handle to btn_ortsraumZero (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Graybild;
global Laplacebild;
global Diffbild;
global lambda;

if (any(isnan(Graybild)))
    msgbox("Graubild nicht geladen");
    return
end

tic;
Laplacebild = edge_zero(Graybild, [0 1 0;1 -4 1; 0 1 0]);
time=toc;
imshow(Laplacebild, 'Parent', handles.axes_laplace);
set(handles.lb_timeOrtsraumZero, 'String', join(["Zeit im Ortsraum (Zero): ", num2str(time*1e3,5), "ms"], ""));

Diffbild = Graybild - uint8(lambda * Laplacebild);
imshow(Diffbild, 'Parent', handles.axes_diff);



% --- Executes on button press in btn_frequenzraum.
function btn_frequenzraum_Callback(hObject, eventdata, handles)
% hObject    handle to btn_frequenzraum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Graybild;
global Laplacebild;
global Diffbild;
global lambda;

if (any(isnan(Graybild)))
    msgbox("Graubild nicht geladen");
    return
end
size(Graybild)

tic;
Laplacebild = edge_frequency(Graybild, [0 1 0;1 -4 1; 0 1 0]);
size(Laplacebild)
time=toc;
imshow(Laplacebild, 'Parent', handles.axes_laplace);
set(handles.lb_timeFrequenzraum, 'String', join(["Zeit im Frequenzraum: ", num2str(time*1e3,5), "ms"], ""));

Diffbild = Graybild - uint8(lambda * Laplacebild(2:end-1, 2:end-1));
imshow(Diffbild, 'Parent', handles.axes_diff);
