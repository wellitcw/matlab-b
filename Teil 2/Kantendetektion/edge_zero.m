function [outPic] = edge_zero(inPic,filter)

if ~exist('inPic', 'var') || ~exist('filter', 'var')
    error('use parameters as following: edge_ignore(inPic,filter)');
end

if ~ismatrix(inPic)
    error('inPic must be 2-dimensional Array');
end

if ~ismatrix(filter) || size(filter, 1) ~= size(filter, 2) || mod(length(filter), 2) == 0
    error('filter must be an square array with odd number of element either way');
end

n = length(filter);
a = (n+1)/2;
outPic = zeros(size(inPic));

extendedPic = padarray(int16(inPic), [a a], 'both');

for x=1:size(outPic, 1)
    for y=1:size(outPic, 2)
        
        for i=1:n
            for j=1:n
                outPic(x, y) = outPic(x, y) + extendedPic(x+i-a+1, y+j-a+1)*filter(n+1-i, n+1-j);
            end
        end
        
    end
end


end

