function [output] = extractM(image)

if ~exist('image', 'var') || ndims(image) ~= 3
    error('parameter must be 3-dimensional array representing a RGB color image');
end

output= bitand(image(:,:,1),7) * 2^5 + bitand(image(:,:,2),3) * 2^3 + bitand(image(:,:,3),7);
output= reshape(output, [numel(output) 1]);
output = output(1:find(output,1,'last'));

end
