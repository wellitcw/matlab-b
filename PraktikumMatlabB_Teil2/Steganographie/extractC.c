#include "mex.h"
#include "string.h"
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    if(nrhs!=1) {
        mexErrMsgTxt("Exactly one input required!");
    }
    if(nrhs>1) {
        mexErrMsgTxt("Too many output arguments!");
    }
    if (!mxIsUint8(prhs[0]) || mxGetNumberOfDimensions(prhs[0]) != 3 || mxGetDimensions(prhs[0])[2] != 3 ) {
        mexErrMsgTxt("parameter must be 3-dimensional array representing a RGB color image");
    }
    
    
    int numel = mxGetDimensions(prhs[0])[0]*mxGetDimensions(prhs[0])[1];;//height*width
    
    
    unsigned char *image = (unsigned char*) mxGetPr(prhs[0]);
    mxArray *tempArray = mxCreateNumericMatrix(1,numel,mxUINT8_CLASS,mxREAL);
    unsigned char *temp = (unsigned char*) mxGetPr(tempArray);
    int numberOfChar = 0;
    
    for (;numberOfChar<numel;numberOfChar++) {      
        temp[numberOfChar] = ((image[numberOfChar] & 0b111) << 5) | ((image[numberOfChar + numel] & 0b011) << 3) | (image[numberOfChar + numel * 2] & 0b111);
        if (temp[numberOfChar] == 0) {
            break;
        }
    }
    
    plhs[0] = mxCreateNumericMatrix(1,numberOfChar,mxUINT8_CLASS,mxREAL);
    memcpy(mxGetPr(plhs[0]), temp, numberOfChar);
    mxDestroyArray(tempArray);
}