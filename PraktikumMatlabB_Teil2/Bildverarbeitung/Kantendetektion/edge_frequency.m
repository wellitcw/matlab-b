function [outPic] = edge_frequency(inPic,filter,direction)
% to prevent cyclic convolution use proper transformation length
NM = size(inPic) +size(filter) - [1 1];
F=fft2(inPic, NM(1), NM(2));
L=fft2(filter, NM(1), NM(2));
G=F.*L;
outPic=ifft2(G);%, size(inPic, 1), size(inPic, 2));
end

