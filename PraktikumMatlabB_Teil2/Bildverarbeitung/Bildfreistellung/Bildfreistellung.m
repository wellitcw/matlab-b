function varargout = Bildfreistellung(varargin)
% BILDFREISTELLUNG MATLAB code for Bildfreistellung.fig
%      BILDFREISTELLUNG, by itself, creates a new BILDFREISTELLUNG or raises the existing
%      singleton*.
%
%      H = BILDFREISTELLUNG returns the handle to a new BILDFREISTELLUNG or the handle to
%      the existing singleton*.
%
%      BILDFREISTELLUNG('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BILDFREISTELLUNG.M with the given input arguments.
%
%      BILDFREISTELLUNG('Property','Value',...) creates a new BILDFREISTELLUNG or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Bildfreistellung_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Bildfreistellung_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Bildfreistellung

% Last Modified by GUIDE v2.5 10-Apr-2019 13:36:46

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Bildfreistellung_OpeningFcn, ...
                   'gui_OutputFcn',  @Bildfreistellung_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Bildfreistellung is made visible.
function Bildfreistellung_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Bildfreistellung (see VARARGIN)

% Choose default command line output for Bildfreistellung
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

global limit;
global Greenbild;
global Backbild;
global Merged;

Greenbild = NaN;
Backbild = NaN;
Merged = NaN;

limit = 170;

% UIWAIT makes Bildfreistellung wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Bildfreistellung_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in btn_loadGreen.
function btn_loadGreen_Callback(hObject, eventdata, handles)
% hObject    handle to btn_loadGreen (see GCBO)
% eventdata  reserved - to be defined5 in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Greenbild;

[pic_file_name, pic_file_path] = uigetfile ({ '*.jpg;*.jpeg','JPEG';'*.png','PNG' }, 'Pic an image');
if ~ischar(pic_file_name) || ~ischar(pic_file_path)
    return
end

Greenbild = imread([pic_file_path, pic_file_name]);
%imshow(Greenbild, 'Parent', handles.axes_Green);
axes(handles.axes_Green);
imshow(Greenbild);



% --- Executes on button press in btn_loadBack.
function btn_loadBack_Callback(hObject, eventdata, handles)
% hObject    handle to btn_loadBack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Backbild;

[pic_file_name, pic_file_path] = uigetfile ({ '*.jpg;*.jpeg','JPEG';'*.png','PNG' }, 'Pic an image');
if ~ischar(pic_file_name) || ~ischar(pic_file_path)
    return
end

Backbild = imread([pic_file_path, pic_file_name]);
imshow(Backbild, 'Parent', handles.axes_Back);


% --- Executes on button press in btn_Merge.
function btn_Merge_Callback(hObject, eventdata, handles)
% hObject    handle to btn_Merge (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Greenbild;
global Backbild;
global Merged;
global limit;

if (any(isnan(Greenbild)))
    msgbox("Greenscreen-Bild nicht geladen");
    return
elseif (any(isnan(Backbild)))
    msgbox("Hintergrund-Bild nicht geladen");
    return
elseif (any(size(Greenbild)~=size(Backbild))) || (numel(Greenbild)==1)
    msgbox("Dimensionen passen nicht");
    return
end

[num, status] = str2num(get(handles.tb_limit,'String'));
if status == 0
    return;
end
limit = num;

start_time = tic;
Merged = thresholdFilter(Greenbild, Backbild, limit);
delta_time=toc(start_time);
imshow(Merged, 'Parent', handles.axes_Merged);
set(handles.lb_timeNotParallel, 'String', join(["Zeit ohne Parallelität: ", num2str(delta_time*1e3,5), " ms"], ""));

% --- Executes on button press in btn_MergeParallel.
function btn_MergeParallel_Callback(hObject, eventdata, handles)
% hObject    handle to btn_MergeParallel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Greenbild;
global Backbild;
global Merged;
global limit;

if (any(isnan(Greenbild)))
    msgbox("Greenscreen-Bild nicht geladen");
    return
elseif (any(isnan(Backbild)))
    msgbox("Hintergrund-Bild nicht geladen");
    return
elseif (any(size(Greenbild)~=size(Backbild))) || (numel(Greenbild)==1)
    msgbox("Dimensionen passen nicht");
    return
end

[num, status] = str2num(get(handles.tb_limit,'String'));
if status == 0
    return;
end

start_time = tic;
Merged = zeros(size(Greenbild), 'uint8'); % reserviere Speicher
parfor loopvar = 1:size(Greenbild, 1)
    Merged(loopvar,:,:) = thresholdFilter(Greenbild(loopvar,:,:), Backbild(loopvar,:,:), limit);
end
delta_time=toc(start_time);
imshow(Merged, 'Parent', handles.axes_Merged);
set(handles.lb_timeParallel, 'String', join(["Zeit mit Parallelität: ", num2str(delta_time*1e3,5), " ms"], ""));



function tb_limit_Callback(hObject, eventdata, handles)
% hObject    handle to tb_limit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tb_limit as text
%        str2double(get(hObject,'String')) returns contents of tb_limit as a double


% --- Executes during object creation, after setting all properties.
function tb_limit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tb_limit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
